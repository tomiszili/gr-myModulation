#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 
# Copyright 2016 Tamás Szili.
# 
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
# 
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
# 


import numpy
from gnuradio import gr


class my_constellation_decision_cb(gr.sync_block):
    """
    docstring for block my_constellation_decision_cb
    """
    def __init__(self, constellation):
        gr.sync_block.__init__(self,
            name="my_constellation_decision_cb",
            in_sig=[numpy.complex64],
            out_sig=[numpy.uint8, numpy.float32, numpy.complex64])

        self.constellation = numpy.array(constellation)
        self.constellation_column = numpy.reshape(self.constellation, (-1, 1))

    def work(self, input_items, output_items):

        in0 = input_items[0]
        out0 = output_items[0]
        out1 = output_items[1]
        out2 = output_items[2]

        self.min_args = numpy.array([0 for i in range(len(in0))])

        # <+signal processing here+>
        distances = numpy.absolute(numpy.subtract(in0, self.constellation_column))
        self.min_args = numpy.argmin(distances, axis=0)

        out0[:] = self.min_args
        out1[:] = 10 * numpy.log10(distances.min(0))
        out2[:] = self.constellation[self.min_args]

        return len(output_items[0])
