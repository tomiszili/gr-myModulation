#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2016 Tamás Szili.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#


from gnuradio import digital

from common_mod_demod import common_modulator
import stellacio


_def_m_ary = 8
_def_samples_per_symbol = 2
_def_excess_bw = 0.35


class mySTARmodulator(common_modulator):
    """
    docstring for block mySTARmodulator
    """

    def __init__(
            self,
            m_ary,
            samples_per_symbol,
            excess_bw,
            pilot_symbol_freq
    ):

        self.m_ary = m_ary
        self.samples_per_symbol = samples_per_symbol
        self.excess_bw = excess_bw
        self.pilot_symbol_freq = pilot_symbol_freq
        self.constellation = self.allocate_STAR_constellation(self.m_ary)

        super(mySTARmodulator, self).__init__(
            constellation=self.constellation,
            samples_per_symbol=self.samples_per_symbol,
            excess_bw=self.excess_bw,
            pilot_symbol_freq=self.pilot_symbol_freq
        )


    def allocate_STAR_constellation(self, constellation_size):
        constellation, size = stellacio.star_constellation(constellation_size)
        constellation_points = [
            constellation[i].I+1j*constellation[i].Q
            for i in range(constellation_size)
            ]
        constellation_symbols = [
            constellation[i].gray_origin_number
            for i in range(constellation_size)
            ]
        constellation_map = digital.constellation_calcdist(
            (constellation_points),
            (constellation_symbols),
            2,
            1
        )

        return constellation_map