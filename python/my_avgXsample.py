#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 
# Copyright 2016 <+YOU OR YOUR COMPANY+>.
# 
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
# 
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
# 


import numpy
from gnuradio import gr


class my_avgXsample(gr.decim_block):
    """
    docstring for block my_avgXsample
    """
    def __init__(self, x):
        gr.decim_block.__init__(self,
                                name="my_avgXsample",
                                in_sig=[numpy.complex64],
                                out_sig=[numpy.complex64],
                                decim=x,
                                )
        self.x = x
        self.avg = 0
        self.prev_length = 0


    def work(self, input_items, output_items):
        in0 = input_items[0]
        out = output_items[0]

        # <+signal processing here+>
        for i in range(0, len(in0), self.x):
            self.avg = numpy.average(in0[i : i + self.x])
            out[i / self.x] = self.avg

        return len(output_items[0])
