#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 
# Copyright 2016 <+YOU OR YOUR COMPANY+>.
# 
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
# 
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
# 


import numpy
from gnuradio import gr


class my_compare_and_select(gr.sync_block):
    """
    docstring for block my_compare_and_select
    """
    def __init__(self, threshold, ninputs):
        gr.sync_block.__init__(self,
                               name="my_compare_and_select",
                               in_sig=[numpy.complex64, ] * ninputs,
                               out_sig=[numpy.float32]
                               )
        self.threshold = threshold
        self.ninputs = ninputs
        self.select = 0

    def work(self, input_items, output_items):
        out = output_items[0]
        # <+signal processing here+>

        for i in range(self.ninputs):
            diff = numpy.subtract(numpy.absolute(numpy.average(input_items[i])), self.threshold)
            if diff >= 0.0:
                self.select = i

        out[:] = self.select

        return len(output_items[0])

    def get_select(self):
        return self.select

