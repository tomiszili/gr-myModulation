#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2016 <+YOU OR YOUR COMPANY+>.
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#

from gnuradio import gr, gr_unittest
from gnuradio import blocks
import numpy
import timeit
from my_PSK_mod_py_bc import my_PSK_mod_py_bc


class qa_my_PSK_mod_py_bc (gr_unittest.TestCase):

    def setUp (self):
        self.tb = gr.top_block ()

    def tearDown (self):
        self.tb = None


    def test_BPSK_performance(self):
        # set up fg
        def set_up_and_run_and_print():
            src = blocks.vector_source_b(map(int, numpy.random.randint(0, 2, 8*1024)), False)
            snk1 = blocks.vector_sink_c()
            stella = my_PSK_mod_py_bc(2)


            self.tb.connect(src, stella)
            self.tb.connect(stella, snk1)

            self.tb.run()

        print sorted(timeit.Timer(set_up_and_run_and_print).repeat(repeat=10, number=10))[0:5]
