#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 
# Copyright 2016 Tamás Szili.
# 
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
# 
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
# 


import numpy

from gnuradio import gr
from math import log

from stellacio import star_constellation


class my_STAR_mod_py_bc(gr.decim_block):
    """
    docstring for block my_STAR_mod_py_bc
    """
    def __init__(self, M_ary):
        self.M_ary = M_ary
        self.Constellation = self.allocate_STAR_constellation(self.M_ary)
        self.symbol_length = int(log(self.M_ary, 2))
        self.binary_decode_array = self.generate_decode_array(self.symbol_length)
        gr.decim_block.__init__(self,
                                name="my_STAR_mod_py_bc",
                                in_sig=[numpy.uint8],
                                out_sig=[numpy.complex64],
                                decim=self.symbol_length
                                )

    @staticmethod
    def allocate_STAR_constellation(constellation_size):
        constellation_map = numpy.array([])
        _star = star_constellation(constellation_size)
        for i in range(constellation_size):
            constellation_map = numpy.append(
                constellation_map,
                numpy.array(
                    [_star[i].I +
                     1j * _star[i].Q]
                )
            )
        
        return constellation_map

    @staticmethod
    def generate_decode_array(symbol_length):
        return 1 << numpy.arange(symbol_length - 1, -1 ,-1)

    def work(self, input_items, output_items):
        in0 = input_items[0]
        out = output_items[0]

        # <+signal processing here+>
        in0 = numpy.reshape(in0, (-1, self.symbol_length))

        out[:] = self.Constellation[
            in0.dot(self.binary_decode_array)
        ]

        return len(output_items[0])
