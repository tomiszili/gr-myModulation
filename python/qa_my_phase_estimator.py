#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 
# Copyright 2016 <+YOU OR YOUR COMPANY+>.
# 
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
# 
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
# 

from gnuradio import gr, gr_unittest
from gnuradio import blocks, digital
import numpy
from my_phase_estimator import my_phase_estimator
import stellacio

class qa_my_phase_estimator (gr_unittest.TestCase):

    def setUp (self):
        self.tb = gr.top_block ()

    def tearDown (self):
        self.tb = None

    def test_001_t(self):
        # set up fg

        constellation = self.allocate_PSK_constellation(8).points()

        index = numpy.random.randint(0, 7, 8 * 3)
        noise = (numpy.random.rand(8 * 3) + 1j * numpy.random.rand(8 * 3)) / 8

        src_data = (
            [constellation[i] + noise[i] for i in index]
        )

        src = blocks.vector_source_c(src_data)
        snk1 = blocks.vector_sink_c()
        stella = my_phase_estimator(constellation)

        self.tb.connect(src, stella)
        self.tb.connect(stella, snk1)
        self.tb.run()
        # check data

        result_data = snk1.data()

        # check data
        # print expected_result
        print result_data

    def allocate_PSK_constellation(self, constellation_size):
        constellation = stellacio.psk_constellation(constellation_size)
        constellation_points = [constellation[i].I + 1j * constellation[i].Q for i in range(constellation_size)]
        constellation_symbols = [constellation[i].gray_origin_number for i in range(constellation_size)]
        constellation_map = digital.constellation_calcdist(
            (constellation_points),
            (constellation_symbols),
            constellation_size,
            1
        )

        return constellation_map


if __name__ == '__main__':
    gr_unittest.run(qa_my_phase_estimator, "qa_my_phase_estimator.xml")
