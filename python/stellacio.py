# QAM and PSK constellation adaptive reserver
# and coordinate points for gray coded constellation points


import collections
from cmath import exp
from math import ceil, floor, log, pi, sqrt


SingleConstellationPoint = collections.namedtuple(
    "SingleConstellationPoint",
    [
        "gray_origin_number",
        "I",
        "Q",
        "gray_code"
    ]
)


def cross_qam(m_ary):
    z_to_squarenumb = int(ceil(sqrt(m_ary)))
    k_act_constel_number = 0
    binbase = log(m_ary, 2)

    while z_to_squarenumb % 2 != 0 or (z_to_squarenumb ** 2 - m_ary) % 2 != 0:
        z_to_squarenumb += 1

    constel_vect = [None for i in range(z_to_squarenumb ** 2)]
    fals_points = sqrt((z_to_squarenumb ** 2 - m_ary) / 4)

    axis_i = z_to_squarenumb - 1
    for i in range(axis_i + 1):

        axis_q = z_to_squarenumb - 1
        for q in range(axis_q + 1):

            if i % 2 == 0:
                asc_index = int(q + z_to_squarenumb * i)
                if (i < fals_points or i > z_to_squarenumb - 1 - fals_points) and\
                        (q < fals_points or q > z_to_squarenumb - 1 - fals_points):

                    constel_vect[asc_index] = SingleConstellationPoint(
                        "NaN",
                        "NaN",
                        "NaN",
                        "NaN"
                    )

                else:
                    constel_vect[asc_index] = SingleConstellationPoint(
                        k_act_constel_number,
                        -axis_i,
                        axis_q,
                        gray(k_act_constel_number, binbase)
                    )
                    k_act_constel_number += 1

            else:
                desc_index = int(z_to_squarenumb * (i + 1) - q - 1)
                if (i < fals_points or i > z_to_squarenumb - 1 - fals_points) and\
                        (q < fals_points or q > z_to_squarenumb - 1 - fals_points):
                    constel_vect[desc_index] = SingleConstellationPoint(
                        "NaN",
                        "NaN",
                        "NaN",
                        "NaN"
                    )

                else:
                    constel_vect[desc_index] = SingleConstellationPoint(
                        k_act_constel_number,
                        -axis_i,
                        -axis_q,
                        gray(k_act_constel_number, binbase)
                    )
                    k_act_constel_number += 1

            axis_q -= 2
        axis_i -= 2

    # print "Cross Constellation"
    return constel_vect


def dec_to_bin(number, bit):
    return bin(number)[2:].zfill(bit)


def gray(number, bit):
    bin_number = dec_to_bin(int(number), int(bit))
    bin_string = []
    for digit in bin_number:
        bin_string.append(int(digit))
    gray_string = []
    for i in range(len(bin_string)):
        if i == 0:
            gray_string.append(bin_string[i])
        else:
            gray_string.append(bin_string[i-1] ^ bin_string[i])
    return "".join(str(e) for e in gray_string)


def rectangular_qam(m_ary):
    x_size = int(floor(sqrt(m_ary)))
    y_size = int(round(m_ary / x_size))
    binbase = log(m_ary, 2)

    k_act_constel_number = 0

    constel_vect = [[] for i in range(m_ary)]

    axis_i = x_size - 1
    for i in range(axis_i + 1):

        axis_q = y_size - 1
        for q in range(axis_q + 1):
            if i % 2 == 0:
                asc_index = int(q + y_size * i)
                constel_vect[asc_index] = SingleConstellationPoint(
                    k_act_constel_number,
                    -axis_i,
                    axis_q,
                    gray(k_act_constel_number, binbase)
                )

            else:
                desc_index = int(y_size * (i + 1) - q - 1)
                constel_vect[desc_index] = SingleConstellationPoint(
                    k_act_constel_number,
                    -axis_i,
                    -axis_q,
                    gray(k_act_constel_number, binbase)
                )
            k_act_constel_number += 1
            axis_q -= 2
        axis_i -= 2

    # print "Rectangular Constellation"
    return constel_vect


def square_qam(m_ary):
    m_exponent = log(m_ary, 4)
    k_act_constel_number = 0
    binbase = log(m_ary, 2)

    constel_vect = [None for i in range(m_ary)]
    axis_i = int(2 ** m_exponent - 1)
    for i in range(axis_i + 1):

        axis_q = int(2 ** m_exponent - 1)
        for j in range(axis_q + 1):
            if i % 2 == 0:
                asc_index = int(j + (2 ** m_exponent * i))
                constel_vect[asc_index] = SingleConstellationPoint(
                    k_act_constel_number,
                    -axis_i,
                    axis_q,
                    gray(k_act_constel_number, binbase)
                )

            else:
                desc_index = int(2 ** m_exponent * (i + 1) - j - 1)
                constel_vect[desc_index] = SingleConstellationPoint(
                    k_act_constel_number,
                    -axis_i,
                    -axis_q,
                    gray(k_act_constel_number, binbase)
                )

            k_act_constel_number += 1
            axis_q -= 2
        axis_i -= 2

    # print "Square Constellation"
    return constel_vect


def psk_constellation(m_ary):
    if m_ary == 2:
        pure_psk = [
            SingleConstellationPoint(0, 1, 0, "0"),
            SingleConstellationPoint(1, -1, 0, "1"),
        ]
        return pure_psk

    else:
        constel = qam_constellation(m_ary)
        constel.sort()

        pure_psk = [None for i in range(0, m_ary)]
        phase_offset = pi / m_ary

        for i in range(len(constel)):
            newindex = int(constel[i].gray_code, 2)
            pure_psk[newindex] = SingleConstellationPoint(
                constel[i].gray_origin_number,
                round(exp((2 * float(i) * pi / m_ary + phase_offset) * 1j).real, 4),
                round(exp((2 * float(i) * pi / m_ary + phase_offset) * 1j).imag, 4),
                constel[i].gray_code
            )

        return pure_psk


def qam_constellation(m_ary):
    if log(m_ary, 2) % 1 == 0:

        if sqrt(m_ary) % 1 == 0:
            constel = square_qam(m_ary)

        elif sqrt(m_ary) % 1 != 0:
            if m_ary > 8:
                constel = cross_qam(m_ary)

            else:
                constel = rectangular_qam(m_ary)

        pure_qam = [None for i in range(0, m_ary)]

        # filter out NaN and sort by gray_code
        for i in range(len(constel)):
            if constel[i].gray_origin_number != "NaN":
                newindex = int(constel[i].gray_code, 2)
                pure_qam[newindex] = constel[i]

        return pure_qam

    else:
        print "M must be equal as an integer power of 2!"
        return


def star_constellation(m_ary):
    if log(m_ary, 2) % 1 == 0:

        constel = []
        if m_ary == 8:
            H = 1
            h = H*sqrt(3)/2
            constel = [
                SingleConstellationPoint(0, -h, 1.5 * H, "000"),
                SingleConstellationPoint(1, -2 * h, 0, "001"),
                SingleConstellationPoint(3, 0, -3 * H, "010"),
                SingleConstellationPoint(2, -h, -1.5 * H, "011"),
                SingleConstellationPoint(7, 0, 3 * H, "100"),
                SingleConstellationPoint(6, h, 1.5 * H, "101"),
                SingleConstellationPoint(4, h, -1.5 * H, "110"),
                SingleConstellationPoint(5, 2 * h, 0, "111"),
                ]

        else:
            print "Star modulator not implemented for this constellation number!"

        pure_star = [None for i in range(0, m_ary)]

        for i in range(len(constel)):
            newindex = int(constel[i].gray_code, 2)
            pure_star[newindex] = SingleConstellationPoint(
                constel[i].gray_origin_number,
                round(constel[i].I, 4),
                round(constel[i].Q, 4),
                constel[i].gray_code
            )

        return pure_star

    else:
        print "M must be equal as an integer power of 2!"
        return
