#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 
# Copyright 2016 Tamás Szili.
# 
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
# 
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
#


import imp
import math
import numpy
import threading
import time

from gnuradio import gr
from gnuradio import analog, digital
from gnuradio import blocks
from gnuradio import filter
mod_codes = imp.load_source(
    "mode_codes.py",
    "/usr/local/lib/python2.7/dist-packages/gnuradio/digital/utils/mod_codes.py"
)

from my_constellation_decision_cb import my_constellation_decision_cb
from my_avgXsample import my_avgXsample
from my_compare_and_select import my_compare_and_select
from my_phase_estimator import my_phase_estimator


_def_samples_per_symbol = 2
_def_excess_bw = 0.35
_def_insertion_place = 100


class common_modulator(gr.hier_block2):

    def __init__(
            self,
            constellation,
            samples_per_symbol=_def_samples_per_symbol,
            excess_bw=_def_excess_bw,
            pilot_symbol_freq=_def_insertion_place
    ):
        gr.hier_block2.__init__(
            self,
            "common_modulator",
            gr.io_signature(1, 1, gr.sizeof_char),  # Input signature
            gr.io_signature(1, 1, gr.sizeof_gr_complex)   # Output signature
        )

        self.pre_defined_constellation_map = constellation
        self.samples_per_symbol = samples_per_symbol
        self.excess_bw = excess_bw
        self.pilot_symbol_freq = pilot_symbol_freq
        self.pilot_symbol = (1 + 0 * 1j) * numpy.absolute(
            self.pre_defined_constellation_map.points()[0]
        )

        if self.samples_per_symbol < 2:
            raise TypeError(
                "Samples per symbol must be >= 2, is %f" % self.samples_per_symbol
            )

        # turn bytes into k-bit vectors
        self.bytes2chunks = \
            blocks.packed_to_unpacked_bb(
                self.pre_defined_constellation_map.bits_per_symbol(),  # bits per chunk
                gr.GR_MSB_FIRST  # endianness
            )

        # map the pre defined constellation
        self.symbol_mapper = digital.map_bb(
            self.pre_defined_constellation_map.pre_diff_code()  # map
        )

        # collect chunks into symbols
        self.chunks2symbols = digital.chunks_to_symbols_bc(
            self.pre_defined_constellation_map.points()  # symbol table
        )

        self.pilot_is_every_ith_element = self.pilot_symbol_freq

        # demultiplexing
        self.output_stream_parser_x = blocks.stream_to_streams(
            gr.sizeof_gr_complex * 1,  # itemsize
            self.pilot_is_every_ith_element - 1  # n-streams
        )

        # pilot symbol source
        self.vector_source = blocks.vector_source_c(
            (self.pilot_symbol,) *
            self.pilot_is_every_ith_element,  # source vector
            True,  # repeat
            1,  # vector length
            []  # stream tags
        )

        # multiplexing
        self.mux_pilot_and_all_parsed_element = blocks.streams_to_stream(
            gr.sizeof_gr_complex * 1,  # itemsize
            self.pilot_is_every_ith_element  # n-streams
        )

        # pulse shaping filter
        nfilts = 32
        ntaps = nfilts * 11 * int(self.samples_per_symbol)  # make nfilts filters of ntaps each
        self.rrc_taps = filter.firdes.root_raised_cosine(
            nfilts,  # gain
            nfilts,  # sampling rate based on 32 filters in resampler
            1.0,  # symbol rate
            self.excess_bw,  # excess bandwidth (roll-off factor)
            ntaps  # taps
        )

        # resampling RRC filter
        self.rrc_filter = filter.pfb_arb_resampler_ccf(
            self.samples_per_symbol,  # rate
            self.rrc_taps  # filter taps
        )

        # Connect
        self.connect_blocks()

    def connect_blocks(self):
        _blocks = [
            self,
            self.bytes2chunks,
            self.symbol_mapper,
            self.chunks2symbols,
        ]

        # connect symbols to demux
        self.connect(
            (self.chunks2symbols, 0),
            (self.output_stream_parser_x, 0)
        )

        # connect pilot source to mux
        self.connect(
            (self.vector_source, 0),
            (self.mux_pilot_and_all_parsed_element, 0)
        )

        # connect demux ports to mux
        for i in range(0, self.pilot_is_every_ith_element - 1):
            self.connect(
                (self.output_stream_parser_x, i),
                (self.mux_pilot_and_all_parsed_element, i + 1)
            )

        # connect mux to RRC filter
        self.connect(
            (self.mux_pilot_and_all_parsed_element, 0),
            (self.rrc_filter, 0)
        )

        _blocks_2 = [
            self.rrc_filter,
            self
        ]
        self.connect(*_blocks)
        self.connect(*_blocks_2)


# symbol timing recovery
_def_timing_bw = 2*math.pi/100.0
_def_timing_max_dev = 1.5


class common_demodulator(gr.hier_block2):

    def __init__(
            self,
            constellation,
            samples_per_symbol=_def_samples_per_symbol,
            excess_bw=_def_excess_bw,
            timing_bw=_def_timing_bw,
            pilot_symbol_freq=_def_insertion_place
    ):
        gr.hier_block2.__init__(
            self,
            "common_demodulator",
            gr.io_signature(1, 1, gr.sizeof_gr_complex),  # Input signature
            gr.io_signaturev(8, 8, [
                gr.sizeof_char,
                gr.sizeof_float,
                gr.sizeof_gr_complex,
                gr.sizeof_gr_complex,
                gr.sizeof_gr_complex,
                gr.sizeof_gr_complex,
                gr.sizeof_gr_complex,
                gr.sizeof_gr_complex
            ])  # Output signature
        )

        self.pre_defined_constellation_map = constellation
        self.samples_per_symbol = samples_per_symbol
        self.excess_bw = excess_bw
        self.timing_bw = timing_bw
        self.timing_max_dev = _def_timing_max_dev
        self.pilot_symbol_freq = pilot_symbol_freq
        self.pilot_symbol = (1 + 0 * 1j) * numpy.absolute(
            self.pre_defined_constellation_map.points()[0]
        )
        self.avg = 32
        self.threshold = 0.8

        if self.samples_per_symbol < 2:
            raise TypeError(
                "Samples per symbol must be >= 2, is %f" % self.samples_per_symbol
            )

        # automatic gain control
        self.agc = analog.agc2_cc(
            1e-1,  # attack rate
            1e-3,  # decay rate
            1,  # reference
            1  # gain
        )

        nfilts = 32
        ntaps = nfilts * 11 * int(self.samples_per_symbol)  # make nfilts filters of ntaps each

        # RRC filter
        self.rrc_taps = filter.firdes.root_raised_cosine(
            nfilts,  # gain
            nfilts * self.samples_per_symbol,  # sampling rate
            1.0,  # symbol rate
            self.excess_bw,  # excess bandwidth (roll-off factor)
            ntaps  # taps
        )

        # time recovery
        self.time_recov = digital.pfb_clock_sync_ccf(
            self.samples_per_symbol,  # samples per symbol
            self.timing_bw,  # loop bandwidth
            self.rrc_taps,  # taps
            nfilts,  # filter size
            nfilts / 2,  # initial phase
            self.timing_max_dev,  # maximum rate deviation
            1   # output samples per symbol
        )

        # CMA EQ
        self.cma_equalizer = digital.cma_equalizer_cc(
            nfilts,  # taps
            1,  # modulus
            1e-3,  # gain
            1  # samples per symbol
        )

        self.pilot_is_every_ith_element = self.pilot_symbol_freq

        # demultiplexing
        self.input_stream_parser_x_last_is_pilot = blocks.stream_to_streams(
            gr.sizeof_gr_complex * 1,  # itemsize
            self.pilot_is_every_ith_element  # n-streams
        )

        # average block for the streams
        self.average_blocks = []
        self.interpols_for_avg_blocks = []
        for n in range(self.pilot_is_every_ith_element):
            self.average_blocks.append(
                my_avgXsample(
                    self.avg
                )
            )

            self.interpols_for_avg_blocks.append(
                filter.interp_fir_filter_ccc(
                    self.avg,  # interpolation
                    ([1, ] * self.avg)  # taps
                )
            )

        # find the stream channel number where the pilot symbols come from
        self.selector = my_compare_and_select(
            self.threshold,  # threshold
            self.pilot_is_every_ith_element  # n-inputs
        )

        # delay the samples for the right demuxing order
        self.delay_val = self.pilot_is_every_ith_element
        self.delay_samples = blocks.delay(
            gr.sizeof_gr_complex * 1,  # itemsize
            self.delay_val  # delay
        )

        # probe for the selector
        def _delay_probe():
            while True:
                val = self.selector.get_select()
                if val != self.pilot_is_every_ith_element - 1:
                    try:
                        self.set_delay(self.delay_val - 1 - val)
                    except AttributeError:
                        pass

                time.sleep(2.0)

        _delay_thread = threading.Thread(target=_delay_probe)
        _delay_thread.daemon = True
        _delay_thread.start()

        self.probe = blocks.probe_signal_f()

        # multiplexing the original data
        self.mux_data = blocks.streams_to_stream(
            gr.sizeof_gr_complex * 1,  # itemsize
            self.pilot_is_every_ith_element - 1  # n-streams
        )

        # interpolation for sync match
        self.interpol = filter.interp_fir_filter_ccc(
            self.pilot_is_every_ith_element - 1,  # interpolation
            ([1, ] * (self.pilot_is_every_ith_element - 1))  # taps
        )

        # phase estimation
        self.constellation_phase_estimator = my_phase_estimator(
            [self.pilot_symbol]
        )

        self.multiply = blocks.multiply_cc()

        # LMS-DD EQ
        self.lms_equalizer = digital.lms_dd_equalizer_cc(
            nfilts,  # number of taps
            1e-6,  # gain
            1,  # samples per symbol
            self.pre_defined_constellation_map.base()  # constellation pointer
        )

        # decision making
        self.decision_maker = my_constellation_decision_cb(
            self.pre_defined_constellation_map.points()
        )

        # map the pre defined constellation
        self.symbol_mapper = digital.map_bb(
            mod_codes.invert_code(
                self.pre_defined_constellation_map.pre_diff_code()  # map
            )
        )

        # unpack the k bit vector into a stream of bits
        self.unpack = blocks.unpack_k_bits_bb(
            self.pre_defined_constellation_map.bits_per_symbol()  # k
        )

        # Connect
        self.connect_blocks()

    def set_delay(self, d):
        self.delay_val = d
        self.delay_samples.set_dly(self.delay_val)

    def connect_blocks(self):
        _blocks = [
            self,
            self.agc,
            self.time_recov,
            self.delay_samples,
            self.cma_equalizer,
        ]

        # connect CMA EQ with demux
        self.connect(
            (self.cma_equalizer, 0),
            (self.input_stream_parser_x_last_is_pilot, 0)
        )

        # connect demux with the avg blocks
        for i in range(self.pilot_is_every_ith_element):
            self.connect(
                (self.input_stream_parser_x_last_is_pilot, i),
                (self.average_blocks[i], 0)
            )

        # connect avg blocks with interpolators
        for i in range(self.pilot_is_every_ith_element):
            self.connect(
                (self.average_blocks[i], 0),
                (self.interpols_for_avg_blocks[i], 0)
            )

        # connect interpolators with the compare and select block
        for i in range(self.pilot_is_every_ith_element):
            self.connect(
                (self.interpols_for_avg_blocks[i], 0),
                (self.selector, i)
            )

        # get the selector value with a probe
        self.connect(
            (self.selector, 0),
            (self.probe, 0)
        )

        # mux the data again
        for i in range(0, self.pilot_is_every_ith_element - 1):
            self.connect(
                (self.input_stream_parser_x_last_is_pilot, i),
                (self.mux_data, self.pilot_is_every_ith_element - 2 - i)
            )

        # the last output from the demux
        self.connect(
            (self.interpols_for_avg_blocks[-1], 0),
            (self.interpol, 0)
        )

        # connect small interpolator with phase estimator
        self.connect(
            (self.interpol, 0),
            (self.constellation_phase_estimator, 0)
        )

        # connect phase estimator to multiplier
        self.connect(
            (self.constellation_phase_estimator, 0),
            (self.multiply, 0)
        )

        # connect mux to multiplier
        self.connect(
            (self.mux_data, 0),
            (self.multiply, 1)
        )

        # connect multiplier to LMS EQ
        self.connect(
            (self.multiply, 0),
            (self.lms_equalizer, 0)
        )

        _blocks_2 = [
            self.lms_equalizer,
            self.decision_maker,
            self.symbol_mapper,
            self.unpack,
            self
        ]

        self.connect(*_blocks)
        self.connect(*_blocks_2)
        self.connect((self.decision_maker, 1), (self, 1))  # EVM
        self.connect((self.decision_maker, 2), (self, 2))  # demod symbol
        self.connect((self.time_recov, 0), (self, 3))  # time recov out
        self.connect((self.cma_equalizer, 0), (self, 4))  # cma out
        self.connect((self.lms_equalizer, 0), (self, 5))  # lms out

        self.connect(
            (self.interpol, 0),
            (self, 6)
        )  # pilot symbol out
        self.connect((self.multiply, 0), (self, 7))  # derotation out
