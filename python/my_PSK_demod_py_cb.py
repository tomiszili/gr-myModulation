#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 
# Copyright 2016 Tamás Szili.
# 
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
# 
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
# 


import numpy

from gnuradio import gr
from math import log, sqrt

from stellacio import psk_constellation

class my_PSK_demod_py_cb(gr.interp_block):
    """
    docstring for block my_PSK_demod_py_cb
    """
    def __init__(self, M_ary):
        self.M_ary = M_ary
        self.Constellation_column = self.allocate_PSK_constellation(self.M_ary)
        self.symbol_length = int(log(M_ary, 2))
        self.gray_array = self.get_PSK_constellation_graystring(self.M_ary)
        gr.interp_block.__init__(self,
                                 name = "my_PSK_demod_py_cb",
                                 in_sig = [numpy.complex64],
                                 out_sig = [numpy.uint8],
                                 interp = self.symbol_length
                                 )

    @staticmethod
    def allocate_PSK_constellation(constellation_size):
        constellation_map = numpy.array([])
        psk_constel = psk_constellation(constellation_size)
        for i in range(constellation_size):
            constellation_map = numpy.append(
                constellation_map,
                numpy.array(
                    [psk_constel[i].I +
                     1j * psk_constel[i].Q]
                )
            )
        return constellation_map.reshape(-1, 1)

    def get_PSK_constellation_graystring (self, constellation_size):
        gray_array = numpy.array([])
        psk_constel = psk_constellation(constellation_size)
        for i in range(constellation_size):
            gray_array = numpy.append(
                gray_array,
                map(int, psk_constel[i].gray_code)
            )
        return gray_array.reshape(-1, self.symbol_length)

    def work(self, input_items, output_items):
        in0 = input_items[0]
        out = output_items[0]

        # <+signal processing here+>
        distances = numpy.absolute(numpy.subtract(in0, self.Constellation_column))
        min_distance_index =  numpy.argmin(distances, axis=0)
        out[:] = self.gray_array[min_distance_index].reshape(1, -1)

        return len(output_items[0])
