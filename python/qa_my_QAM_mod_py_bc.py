#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 
# Copyright 2016 <+YOU OR YOUR COMPANY+>.
# 
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
# 
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
# 

from gnuradio import gr, gr_unittest
from gnuradio import blocks
from my_QAM_mod_py_bc import my_QAM_mod_py_bc

class qa_my_QAM_mod_py_bc (gr_unittest.TestCase):

    def setUp (self):
        self.tb = gr.top_block ()

    def tearDown (self):
        self.tb = None

    def test_001_t (self):
        # set up fg
        scr_data = (
            0,0,0,0,
            0,0,1,0,
            0,1,0,0

        )

        expected_result=(
            (-3.0 + 3.0 * 1j),
            (-3.0 -3.0 * 1j),
            (-1.0 -3.0 * 1j),
        )

        src = blocks.vector_source_b(scr_data)
        stella = my_QAM_mod_py_bc(16)
        snk1 = blocks.vector_sink_c()


        self.tb.connect(src, stella)
        self.tb.connect(stella, snk1)

        self.tb.run()

        result_data = snk1.data()

        # check data
        # print expected_result
        # print result_data
        self.assertComplexTuplesAlmostEqual(expected_result, result_data, 4, "rossz")

    def test_002_t (self):
        # set up fg
        scr_data = (
            0,0,0,0,0,
            0,0,1,0,1,
            0,1,0,0,1
        )

        expected_result=(
            (-5.0 + 3.0 * 1j),
            (-3.0 + 1.0 * 1j),
            (-1.0 - 3.0 * 1j),
        )

        src = blocks.vector_source_b(scr_data)
        stella = my_QAM_mod_py_bc(32)
        snk1 = blocks.vector_sink_c()


        self.tb.connect(src, stella)
        self.tb.connect(stella, snk1)

        self.tb.run()

        result_data = snk1.data()

        # check data
        # print expected_result
        # print result_data
        self.assertComplexTuplesAlmostEqual(expected_result, result_data, 4, "rossz")


if __name__ == '__main__':
    gr_unittest.run(qa_my_QAM_mod_py_bc, "qa_my_QAM_mod_py_bc.xml")
