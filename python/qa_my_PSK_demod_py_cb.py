#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 
# Copyright 2016 <+YOU OR YOUR COMPANY+>.
# 
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
# 
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
# 

from gnuradio import gr, gr_unittest
from gnuradio import blocks
from my_PSK_demod_py_cb import my_PSK_demod_py_cb

class qa_my_PSK_demod_py_cb (gr_unittest.TestCase):

    def setUp (self):
        self.tb = gr.top_block ()

    def tearDown (self):
        self.tb = None

    def test_001_t (self):
        # set up fg
        scr_data = (
            (1. + 0. * 1j),
            (0.3827 + 0.9239 * 1j),
            (-0.9239 + 0.3827 * 1j),

        )
        expected_result=(
            0, 0, 0, 0,
            0, 0, 1, 0,
            0, 1, 0, 0,
        )

        src = blocks.vector_source_c(scr_data)
        stella = my_PSK_demod_py_cb(16)
        snk1 = blocks.vector_sink_b()


        self.tb.connect(src, stella)
        self.tb.connect(stella, snk1)


        self.tb.run()

        result_data = snk1.data()

        # check data
        # print expected_result
        # print result_data
        self.assertComplexTuplesAlmostEqual(expected_result, result_data, 4, "rossz")


if __name__ == '__main__':
    gr_unittest.run(qa_my_PSK_demod_py_cb, "qa_my_PSK_demod_py_cb.xml")
