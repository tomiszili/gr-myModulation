# Make build folder into the root of the module (gr-myModulation) and navigate into it:

mkdir build
cd build

# Then run the installing command:
cmake .. && make && sudo make install && sudo ldconfig
